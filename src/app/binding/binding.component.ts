import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-binding',
  templateUrl: './binding.component.html',
  styleUrls: ['./binding.component.css']
})
export class BindingComponent implements OnInit {

  word:string = "hello";


  logName():void{
    console.log(this.word);
  }
  constructor() { }

  ngOnInit(): void {
  }

}
